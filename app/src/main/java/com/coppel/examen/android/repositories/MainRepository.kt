package com.coppel.examen.android.repositories

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.ui.AppBarConfiguration
import com.coppel.examen.android.R
import com.coppel.examen.android.managers.ResourceManager
import com.coppel.examen.android.models.Character
import com.coppel.examen.android.models.DetailItem
import com.coppel.examen.android.utils.Constants
import com.coppel.examen.android.utils.Constants.FavoriteStateDef
import com.coppel.examen.android.utils.Constants.ItemDef

class MainRepository private constructor(context: Context): NetworkRepository(context) {
    companion object {
        @Volatile
        private var instance: MainRepository? = null

        fun getInstance(context: Context): MainRepository = instance ?: synchronized(this) {
            instance ?: MainRepository(context).also {
                instance = it
            }
        }
    }

    private val destination = MutableLiveData(0)
    private val favoriteState: MutableLiveData<Int> = MutableLiveData(FavoriteStateDef.DEFAULT)

    var query = Constants.EMPTY
    private var isFavoriteActive = false

    val navConfiguration: AppBarConfiguration = AppBarConfiguration.Builder(
        R.id.navigationHome,
        R.id.navigationFavorites
    ).build()

    fun getDestination(): LiveData<Int> {
        return destination
    }

    fun getFavoriteState(): LiveData<Int> {
        return favoriteState
    }

    fun setFavoriteState(@FavoriteStateDef result: Int) {
        this.favoriteState.value = result
        this.favoriteState.postValue(FavoriteStateDef.DEFAULT)
    }

    fun isFavoriteActive(): Boolean {
        return isFavoriteActive
    }

    fun setFavoriteActive(result: Boolean) {
        isFavoriteActive = result
        setFavoriteState(FavoriteStateDef.INVALIDATE_MENU)
    }

    fun getInfo(model: Character): List<DetailItem> {
        val context = ResourceManager.getInstance().getContext()
        val titleNumber = context.getString(R.string.titleNumber)
        val list: MutableList<DetailItem> = ArrayList()

        list.add(DetailItem(context.getString(R.string.titleAppName)))
        list.add(DetailItem(ItemDef.CONTENT, R.drawable.ic_next, context.getString(R.string.titleId), model.id))
        list.add(DetailItem())
        list.add(DetailItem(ItemDef.CONTENT, R.drawable.ic_next, context.getString(R.string.titleName), model.name))
        list.add(DetailItem())

        if(model.description.isEmpty()) {
            list.add(DetailItem(ItemDef.CONTENT, R.drawable.ic_next, context.getString(R.string.titleDescription), context.getString(R.string.textNoInformation)))
        } else {
            list.add(DetailItem(ItemDef.CONTENT, R.drawable.ic_next, context.getString(R.string.titleDescription), model.description))
        }

        list.add(DetailItem(context.getString(R.string.titleComics)))
        val comics = model.comics.items

        if(comics.isEmpty()) {
            list.add(DetailItem(ItemDef.CONTENT, R.drawable.ic_next, context.getString(R.string.textNoInformation), Constants.EMPTY))
            list.add(DetailItem())
        } else {
            comics.forEachIndexed { index, element ->
                val id = index + 1
                list.add(DetailItem(ItemDef.CONTENT, R.drawable.ic_next, "$titleNumber$id:", element.name))
                list.add(DetailItem())
            }
        }

        list.add(DetailItem(context.getString(R.string.titleSeries)))
        val series = model.series.items

        if(series.isEmpty()) {
            list.add(DetailItem(ItemDef.CONTENT, R.drawable.ic_next, context.getString(R.string.textNoInformation), Constants.EMPTY))
            list.add(DetailItem())
        } else {
            series.forEachIndexed { index, element ->
                val id = index + 1
                list.add(DetailItem(ItemDef.CONTENT, R.drawable.ic_next, "$titleNumber$id:", element.name))
                list.add(DetailItem())
            }
        }

        list.add(DetailItem(context.getString(R.string.titleStories)))
        val stories = model.stories.items

        if(stories.isEmpty()) {
            list.add(DetailItem(ItemDef.CONTENT, R.drawable.ic_next, context.getString(R.string.textNoInformation), Constants.EMPTY))
            list.add(DetailItem())
        } else {
            stories.forEachIndexed { index, element ->
                val id = index + 1
                list.add(DetailItem(ItemDef.CONTENT, R.drawable.ic_next, "$titleNumber$id:", element.name))
                list.add(DetailItem())
            }
        }

        list.add(DetailItem(context.getString(R.string.titleEvents)))
        val events = model.events.items

        if(events.isEmpty()) {
            list.add(DetailItem(ItemDef.CONTENT, R.drawable.ic_next, context.getString(R.string.textNoInformation), Constants.EMPTY))
            list.add(DetailItem())
        } else {
            events.forEachIndexed { index, element ->
                val id = index + 1
                list.add(DetailItem(ItemDef.CONTENT, R.drawable.ic_next, "$titleNumber$id:", element.name))
                list.add(DetailItem())
            }
        }

        list.add(DetailItem(context.getString(R.string.titleUrls)))
        val urls = model.urls

        if(events.isEmpty()) {
            list.add(DetailItem(ItemDef.CONTENT, R.drawable.ic_next, context.getString(R.string.textNoInformation), Constants.EMPTY))
            list.add(DetailItem())
        } else {
            urls.forEachIndexed { _, element ->
                list.add(DetailItem(ItemDef.URL, R.drawable.ic_next, context.getString(R.string.titleUrl), element.type, element.url))
                list.add(DetailItem())
            }
        }

        return list
    }
}