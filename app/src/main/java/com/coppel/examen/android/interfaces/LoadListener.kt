package com.coppel.examen.android.interfaces

interface LoadListener {
    fun onLoadMore()
}