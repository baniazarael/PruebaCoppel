package com.coppel.examen.android.models

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
class Items(
    @SerializedName("items")
    val items: MutableList<Item>
)