package com.coppel.examen.android.retrofit.services

import com.coppel.examen.android.retrofit.Endpoints
import com.coppel.examen.android.retrofit.responses.CharactersResponse
import com.coppel.examen.android.utils.Constants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET(Endpoints.URL_CHARACTERS)
    suspend fun search(
        @Query(Endpoints.PARAM_TS)
        ts: Long,

        @Query(Endpoints.PARAM_APIKEY)
        apikey: String = Endpoints.PUBLIC_KEY,

        @Query(Endpoints.PARAM_HASH)
        hash: String,

        @Query(Endpoints.PARAM_LIMIT)
        limit: Int = Constants.LIMIT_SEARCH,

        @Query(Endpoints.PARAM_NAME_STARTS_WITH)
        nameStartsWith: String
    ): Response<CharactersResponse>

    @GET(Endpoints.URL_CHARACTERS_BY_ID)
    suspend fun getCharacter(
        @Path(Endpoints.PARAM_ID)
        id: String,

        @Query(Endpoints.PARAM_TS)
        ts: Long,

        @Query(Endpoints.PARAM_APIKEY)
        apikey: String = Endpoints.PUBLIC_KEY,

        @Query(Endpoints.PARAM_HASH)
        hash: String
    ): Response<CharactersResponse>

    @GET(Endpoints.URL_CHARACTERS)
    suspend fun getCharacters(
        @Query(Endpoints.PARAM_TS)
        ts: Long,

        @Query(Endpoints.PARAM_APIKEY)
        apikey: String = Endpoints.PUBLIC_KEY,

        @Query(Endpoints.PARAM_HASH)
        hash: String,

        @Query(Endpoints.PARAM_LIMIT)
        limit: Int = Constants.LIMIT_CHARACTERS,

        @Query(Endpoints.PARAM_OFFSET)
        offset: Int,
    ): Response<CharactersResponse>
}