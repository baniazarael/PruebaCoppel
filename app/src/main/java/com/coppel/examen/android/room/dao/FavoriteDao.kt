package com.coppel.examen.android.room.dao

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.*
import com.coppel.examen.android.room.models.Favorite

@Dao
interface FavoriteDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(favorite: Favorite)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(favorites: List<Favorite>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun replace(favorite: Favorite)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun replace(favorites: List<Favorite>)

    @Update
    suspend fun update(favorite: Favorite)

    @Update
    suspend fun update(favorites: List<Favorite>)

    @Delete
    suspend fun delete(favorite: Favorite)

    @Delete
    suspend fun delete(favorites: List<Favorite>)

    @Query("DELETE FROM tableFavorites")
    suspend fun deleteAll()

    @Query("SELECT * FROM tableFavorites WHERE id = :id LIMIT 1")
    fun getFavoriteById(id: String): LiveData<Favorite>

    @Query("SELECT * FROM tableFavorites ORDER BY id")
    fun getFavorites(): PagingSource<Int, Favorite>
}