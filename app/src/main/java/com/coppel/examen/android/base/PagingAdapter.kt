package com.coppel.examen.android.base

import android.annotation.SuppressLint
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.coppel.examen.android.interfaces.ItemListener
import com.coppel.examen.android.viewHolders.BindingHolder

abstract class PagingAdapter<R: Any>(
    diffCallback: DiffUtil.ItemCallback<R>,
    val listener: ItemListener<R>
): PagingDataAdapter<R, BindingHolder>(diffCallback) {
    private val loadStateListener = { combinedLoadStates: CombinedLoadStates ->
        val loadState = combinedLoadStates.source.refresh

        if(loadState is LoadState.NotLoading) {
            val itemCount = itemCount
            listener.onDataSet(itemCount == 0, itemCount)
        }
    }

    init {
        addLoadStateListener(loadStateListener)
    }

    open fun getContent(position: Int): R? {
        return super.getItem(position)
    }

    @SuppressLint("NotifyDataSetChanged")
    open fun update() {
        notifyDataSetChanged()
    }
}