package com.coppel.examen.android.ui.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import com.coppel.examen.android.R
import com.coppel.examen.android.databinding.DialogErrorBinding

class ErrorDialog private constructor(
    context: Context,
): Dialog(context) {
    private lateinit var binding: DialogErrorBinding

    private var title = ""
    private var text = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        binding = DialogErrorBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnAccept.setOnClickListener {
            cancel()
        }

        setContent()
    }

    override fun show() {
        super.show()

        setContent()
    }

    fun setContent(title: String, text: String) {
        this.title = title
        this.text = text
    }

    private fun setContent() {
        binding.tvTitle.text = title
        binding.tvText.text = text
    }

    class Builder(private val context: Context) {
        private val dialog: ErrorDialog = ErrorDialog(context)

        fun create(text: String) {
            dialog.setContent(
                context.getString(R.string.titleError),
                text
            )

            if(!dialog.isShowing) {
                dialog.show()
            }
        }

        fun create(title: String, text: String) {
            dialog.setContent(title, text)

            if(!dialog.isShowing) {
                dialog.show()
            }
        }

        fun create(text: Int) {
            dialog.setContent(
                context.getString(R.string.titleError),
                context.getString(text)
            )

            if(!dialog.isShowing) {
                dialog.show()
            }
        }

        fun create(title: Int, text: Int) {
            dialog.setContent(
                context.getString(title),
                context.getString(text)
            )

            if(!dialog.isShowing) {
                dialog.show()
            }
        }
    }
}