package com.coppel.examen.android.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.coppel.examen.android.models.Character
import com.coppel.examen.android.repositories.MainRepository
import com.coppel.examen.android.room.models.Favorite
import com.coppel.examen.android.utils.Constants.FavoriteStateDef
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

open class DaoViewModel(application: Application): AndroidViewModel(application) {
    protected val repository = MainRepository.getInstance(application.applicationContext)

    val favorites: Flow<PagingData<Favorite>> = repository.favorites.cachedIn(viewModelScope)

    fun delete(favorite: Favorite) {
        viewModelScope.launch {
            repository.delete(favorite)
        }
    }

    fun addFavorite(character: Character) {
        viewModelScope.launch {
            repository.replace(Favorite(character.id, character.name, character.thumbnail.getUrl()))
            repository.setFavoriteState(FavoriteStateDef.ADD_FAVORITE)
            repository.setFavoriteActive(true)
        }
    }

    fun setFavoriteActive(result: Boolean) {
        this.repository.setFavoriteActive(result)
    }

    fun isFavorite(id: String): LiveData<Favorite> {
        return repository.isFavorite(id)
    }
}