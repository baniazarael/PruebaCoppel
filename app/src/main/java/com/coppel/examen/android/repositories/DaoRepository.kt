package com.coppel.examen.android.repositories

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.paging.Pager
import com.coppel.examen.android.room.AppDatabase
import com.coppel.examen.android.room.dao.FavoriteDao
import com.coppel.examen.android.room.models.Favorite

open class DaoRepository(context: Context): BaseRepository() {
    private val database = AppDatabase.getInstance(context)

    private val favoriteDao: FavoriteDao = database.favoriteDao()

    val favorites = Pager(pagingConfig) {
        favoriteDao.getFavorites()
    }.flow

    suspend fun insert(model: Favorite) {
        this.favoriteDao.insert(model)
    }

    suspend fun insert(list: List<Favorite>) {
        this.favoriteDao.insert(list)
    }

    suspend fun replace(model: Favorite) {
        this.favoriteDao.replace(model)
    }

    suspend fun replace(list: List<Favorite>) {
        this.favoriteDao.replace(list)
    }

    suspend fun update(model: Favorite) {
        this.favoriteDao.update(model)
    }

    suspend fun update(list: List<Favorite>) {
        this.favoriteDao.update(list)
    }

    suspend fun delete(model: Favorite) {
        this.favoriteDao.delete(model)
    }

    suspend fun delete(list: List<Favorite>) {
        this.favoriteDao.delete(list)
    }

    suspend fun deleteAll() {
        this.favoriteDao.deleteAll()
    }

    fun isFavorite(id: String): LiveData<Favorite> {
        return favoriteDao.getFavoriteById(id)
    }
}