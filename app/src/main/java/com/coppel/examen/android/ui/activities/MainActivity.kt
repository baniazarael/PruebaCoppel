package com.coppel.examen.android.ui.activities

import android.app.SearchManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.SearchView
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.coppel.examen.android.R
import com.coppel.examen.android.databinding.ActivityMainBinding
import com.coppel.examen.android.managers.NetworkManager.NetworkStateDef
import com.coppel.examen.android.ui.dialogs.ErrorDialog
import com.coppel.examen.android.ui.dialogs.LoadingDialog
import com.coppel.examen.android.ui.dialogs.NoConnectionDialog
import com.coppel.examen.android.utils.LogUtils
import com.coppel.examen.android.viewModels.MainViewModel
import com.coppel.examen.android.viewModels.ViewModelFactory

class MainActivity: AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private lateinit var navController: NavController

    private val viewModel by viewModels<MainViewModel> {
        ViewModelFactory()
    }

    private lateinit var errorBuilder: ErrorDialog.Builder
    private lateinit var loadingBuilder: LoadingDialog.Builder
    private lateinit var noConnectionBuilder: NoConnectionDialog.Builder

    private val callback = object: OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            val navDestination = navController.currentDestination ?: return
            when (navDestination.id) {
                R.id.navigationHome,
                R.id.navigationFavorites -> {
                    finish()
                }

                else -> {
                    navController.popBackStack()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.layoutToolbar.toolbar)
        val actionBar = supportActionBar

        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeButtonEnabled(true)
            binding.layoutToolbar.toolbar.navigationIcon = null
        }

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment? ?: throw RuntimeException("NavHostFragment is null")

        navController = navHostFragment.navController
        navController.addOnDestinationChangedListener { _, destination, _ ->
            manageDestination(destination)
        }

        binding.layoutToolbar.appBarLayout.outlineProvider = null
        NavigationUI.setupActionBarWithNavController(this, navController, viewModel.navConfiguration)
        NavigationUI.setupWithNavController(binding.bottomNavigationView, navController)

        loadingBuilder = LoadingDialog.Builder(this)

        binding.bottomNavigationView.setOnItemReselectedListener {

        }

        viewModel.destination.observe(this) { destination ->
            if(destination == 0) {
                return@observe
            }

            navController.navigate(destination)
        }

        viewModel.updating.observe(this) { updating ->
            if(updating) {
                loadingBuilder.create()
            } else {
                loadingBuilder.cancel()
            }
        }

        viewModel.error.observe(this) {
            if(it == null) {
                return@observe
            }

            errorBuilder.create(R.string.textError)
        }

        viewModel.networkState.observe(this) {
            if(it == NetworkStateDef.DISCONNECTED) {
                noConnectionBuilder.create()
            }
        }

        viewModel.network.observe(this) { integer ->
            when (integer) {
                NetworkStateDef.DEFAULT -> {
                    LogUtils.print("Default case for initialize NetworkCheck")
                }

                NetworkStateDef.CONNECTED -> {

                }

                NetworkStateDef.DISCONNECTED -> {

                }
            }
        }

        binding.fab.setOnClickListener {
            val searchView: View = findViewById(R.id.actionSearch)
            searchView.performClick()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        val searchItem = menu.findItem(R.id.actionSearch)
        searchItem.setOnActionExpandListener(object: MenuItem.OnActionExpandListener {
            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                binding.fab.visibility = View.VISIBLE
                viewModel.requestCharacters(true)
                return true
            }

            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                binding.fab.visibility = View.GONE
                return true
            }
        })

        val searchManager = getSystemService(SEARCH_SERVICE) as SearchManager
        val searchView = searchItem.actionView as SearchView

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                viewModel.setQuery(query)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                viewModel.setQuery(newText)
                return true
            }
        })

        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val navDestination = navController.currentDestination ?: return false

        when (navDestination.id) {
            R.id.navigationHome -> {
                enableMenu(menu)
            }

            R.id.navigationFavorites -> {
                disableMenu(menu)
            }

            else -> {
                throw RuntimeException("Unknown Nav: " + navDestination.label)
            }
        }

        return true
    }

    override fun onStart() {
        super.onStart()
        onBackPressedDispatcher.addCallback(callback)
    }

    override fun onStop() {
        callback.remove()
        super.onStop()
    }

    private fun enableMenu(menu: Menu) {
        menu.findItem(R.id.actionSearch).isEnabled = true
        menu.findItem(R.id.actionSearch).isVisible = true
    }

    private fun disableMenu(menu: Menu) {
        menu.findItem(R.id.actionSearch).isEnabled = false
        menu.findItem(R.id.actionSearch).isVisible = false
    }

    private fun manageDestination(destination: NavDestination) {
        invalidateOptionsMenu()

        if(destination.id == R.id.navigationFavorites) {
            binding.fab.visibility = View.GONE
        } else {
            binding.fab.visibility = View.VISIBLE
        }

        viewModel.setQuery("")
    }
}