package com.coppel.examen.android.retrofit.responses

import androidx.annotation.Keep
import com.coppel.examen.android.models.Data
import com.google.gson.annotations.SerializedName

@Keep
data class CharactersResponse(
    @SerializedName("code")
    val code: Int,

    @SerializedName("status")
    val status: String,

    @SerializedName("copyright")
    val copyright: String,

    @SerializedName("attributionHTML")
    val attributionHTML: String,

    @SerializedName("data")
    val data: Data
): Response()