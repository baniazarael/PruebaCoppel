package com.coppel.examen.android.retrofit.base

import com.coppel.examen.android.interfaces.NetworkListener
import com.coppel.examen.android.managers.NetworkManager
import com.coppel.examen.android.retrofit.Endpoints
import com.coppel.examen.android.retrofit.managers.RequestManager
import com.coppel.examen.android.retrofit.responses.ErrorResponse
import com.coppel.examen.android.retrofit.responses.Response
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

abstract class RetrofitRequest(
    val TAG: String,
    baseUrl: String
): RequestManager(TAG, baseUrl) {
    constructor(TAG: String): this(TAG, Endpoints.baseUrl)

    private var networkListener: NetworkListener? = null

    open fun setNetworkListener(networkListener: NetworkListener) {
        this.networkListener = networkListener
    }

    fun onError(responseBody: Response?, responseCode: Int): ErrorResponse {
        val content: String = if(responseBody != null) {
            "Code: $responseCode - Response: $responseBody"
        } else {
            "Code: $responseCode"
        }

        return onError(RuntimeException(content))
    }

    fun onError(ex: Exception): ErrorResponse {
        return ErrorResponse(ex)
    }

    private suspend fun onConnected() {
        withContext(Dispatchers.Main) {
            networkListener?.onConnected()
        }
    }

    private suspend fun onDisconnected() {
        withContext(Dispatchers.Main) {
            networkListener?.onDisconnected()
        }
    }

    override suspend fun onPreExecute(): Boolean {
        return if(NetworkManager.isNetworkConnected()) {
            onConnected()
            true
        } else {
            onDisconnected()
            false
        }
    }
}