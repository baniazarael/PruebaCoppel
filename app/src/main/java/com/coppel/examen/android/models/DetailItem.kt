package com.coppel.examen.android.models

import androidx.annotation.DrawableRes
import com.coppel.examen.android.utils.Constants.ItemDef

data class DetailItem(
    @ItemDef
    val id: Int,

    @DrawableRes
    val icon: Int,

    val title: String,

    val text: String,

    val url: String? = null
) {
    constructor(title: String): this(ItemDef.HEADER, 0, title, "", null)

    constructor(): this(ItemDef.DIVIDER, 0, "", "", null)
}