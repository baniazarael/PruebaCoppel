package com.coppel.examen.android.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.coppel.examen.android.room.dao.FavoriteDao
import com.coppel.examen.android.room.models.Favorite

@Database(
    entities = [Favorite::class],
    version = RoomConstants.databaseVersion
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun favoriteDao(): FavoriteDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                val it = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    RoomConstants.databaseName
                ).build()

                instance = it
                it
            }
        }
    }
}