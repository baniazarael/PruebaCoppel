package com.coppel.examen.android.utils

import androidx.annotation.IntDef
import androidx.annotation.StringDef

class Constants {
    companion object {
        const val R_CHARACTERS = "Characters"
        const val R_SEARCH = "Search"

        const val LIMIT_SEARCH = 10
        const val LIMIT_CHARACTERS = 20

        const val DELAY_SEARCH = 1000L
        const val INITIAL_OFFSET = 0
        const val EMPTY = ""
        const val RESPONSE_OK = "Ok"
    }

    @Retention(AnnotationRetention.SOURCE)
    @StringDef(
        JsonConstants.id,
        JsonConstants.query,
        JsonConstants.offset
    )
    annotation class JsonConstants {
        companion object {
            const val id = "id"
            const val query = "query"
            const val offset = "offset"
        }
    }

    @Retention(AnnotationRetention.SOURCE)
    @IntDef(
        ItemDef.HEADER,
        ItemDef.DIVIDER,
        ItemDef.CONTENT,
        ItemDef.URL
    )
    annotation class ItemDef {
        companion object {
            const val HEADER = 1
            const val DIVIDER = 2
            const val CONTENT = 3
            const val URL = 4
        }
    }

    @Retention(AnnotationRetention.SOURCE)
    @IntDef(
        ScrollStateDef.DEFAULT,
        ScrollStateDef.LOADED,
        ScrollStateDef.RESUMED,
        ScrollStateDef.PAUSED,
        ScrollStateDef.ENDED
    )
    annotation class ScrollStateDef {
        companion object {
            const val DEFAULT = 1
            const val LOADED = 2
            const val RESUMED = 3
            const val PAUSED = 4
            const val ENDED = 5
        }
    }

    @Retention(AnnotationRetention.SOURCE)
    @IntDef(
        FavoriteStateDef.DEFAULT,
        FavoriteStateDef.INVALIDATE_MENU,
        FavoriteStateDef.ADD_FAVORITE
    )
    annotation class FavoriteStateDef {
        companion object {
            const val DEFAULT = 0
            const val INVALIDATE_MENU = 1
            const val ADD_FAVORITE = 2
        }
    }
}