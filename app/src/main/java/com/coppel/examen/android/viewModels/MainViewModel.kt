package com.coppel.examen.android.viewModels

import android.app.Application
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.coppel.examen.android.models.Character
import com.coppel.examen.android.models.DetailItem
import com.coppel.examen.android.utils.Constants
import kotlinx.coroutines.launch

class MainViewModel(application: Application): RequestViewModel(application) {
    val destination: LiveData<Int> = repository.getDestination()
    val favoriteState: LiveData<Int> = repository.getFavoriteState()
    val network: LiveData<Int> = repository.getNetworkState()

    val navConfiguration = repository.navConfiguration
    val query = repository.query

    fun isFavoriteActive(): Boolean {
        return repository.isFavoriteActive()
    }

    private val handler = Handler(Looper.getMainLooper())

    fun setQuery(query: String) {
        this.repository.query = query

        if(query.isEmpty()) {
            viewModelScope.launch {
                repository.setScrollState(Constants.ScrollStateDef.RESUMED)
            }
        } else {
            this.handler.removeCallbacksAndMessages(null)
            this.handler.postDelayed({
                viewModelScope.launch {
                    repository.requestSearch(query)
                }
            }, Constants.DELAY_SEARCH)
        }
    }

    fun getInfo(character: Character): List<DetailItem> {
        return repository.getInfo(character)
    }
}