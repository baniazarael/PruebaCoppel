package com.coppel.examen.android.models.diff

import androidx.recyclerview.widget.DiffUtil
import com.coppel.examen.android.room.models.Favorite

class DiffUtils {
    companion object {
        val FAVORITE_ITEM_CALLBACK: DiffUtil.ItemCallback<Favorite> = object: DiffUtil.ItemCallback<Favorite>() {
            override fun areItemsTheSame(oldItem: Favorite, newItem: Favorite): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Favorite, newItem: Favorite): Boolean {
                return oldItem == newItem
            }
        }
    }
}