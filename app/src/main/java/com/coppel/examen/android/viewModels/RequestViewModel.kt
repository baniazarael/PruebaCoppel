package com.coppel.examen.android.viewModels

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.coppel.examen.android.models.Character
import com.coppel.examen.android.room.models.Favorite
import kotlinx.coroutines.launch

open class RequestViewModel(application: Application): DaoViewModel(application) {
    val updating: LiveData<Boolean>
        get() = repository.updating

    val error: LiveData<Exception?>
        get() = repository.error

    val networkState: LiveData<Int>
        get() = repository.networkState

    val scrollState: LiveData<Int>
        get() = repository.scrollState

    val characters: LiveData<MutableList<Character>?>
        get() = repository.characters

    val character: LiveData<Character?>
        get() = repository.character

    fun setCharacter(character: Character?) {
        viewModelScope.launch {
            repository.setCharacter(character)
        }
    }

    fun setFavorite(result: Favorite) {
        viewModelScope.launch {
            repository.requestCharacter(result.id)
        }
    }

    fun requestCharacters(init: Boolean = false) {
        viewModelScope.launch {
            repository.requestCharacters(init)
        }
    }
}