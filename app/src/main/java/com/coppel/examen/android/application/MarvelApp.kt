package com.coppel.examen.android.application

import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDexApplication
import com.coppel.examen.android.managers.ImageManager
import com.coppel.examen.android.managers.PreferenceManager
import com.coppel.examen.android.managers.ResourceManager
import com.coppel.examen.android.managers.WriteManager

open class MarvelApp: MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        ResourceManager.getInstance().initialize(this)
        PreferenceManager.getInstance().initialize(this, "Marvel")
        ImageManager.instance.initialize(this)
        WriteManager.initProject(this, "Coppel", "Marvel")
    }
}