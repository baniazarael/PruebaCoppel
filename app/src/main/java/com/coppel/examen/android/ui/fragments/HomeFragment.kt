package com.coppel.examen.android.ui.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.coppel.examen.android.custom.InfiniteScrollListener
import com.coppel.examen.android.databinding.FragmentHomeBinding
import com.coppel.examen.android.interfaces.ItemListener
import com.coppel.examen.android.interfaces.LoadListener
import com.coppel.examen.android.managers.NetworkManager
import com.coppel.examen.android.models.Character
import com.coppel.examen.android.ui.activities.DetailActivity
import com.coppel.examen.android.ui.adapters.HomeAdapter
import com.coppel.examen.android.utils.Constants.ScrollStateDef
import com.coppel.examen.android.viewModels.MainViewModel
import com.coppel.examen.android.viewModels.ViewModelFactory

class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding

    private val viewModel by viewModels<MainViewModel> {
        ViewModelFactory()
    }

    private lateinit var adapter: HomeAdapter
    private lateinit var scrollListener: InfiniteScrollListener

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = HomeAdapter(object : ItemListener<Character> {
            override fun onItemSelected(item: Character) {
                handleItemSelected(item)
            }

            override fun onDataSet(isEmpty: Boolean, itemCount: Int) {
                handleData(isEmpty)
            }
        })

        val layoutManager = LinearLayoutManager(context)
        scrollListener = InfiniteScrollListener(layoutManager, object : LoadListener {
            override fun onLoadMore() {
                viewModel.requestCharacters()
            }
        })

        binding.recyclerView.addOnScrollListener(scrollListener)
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = adapter

        viewModel.characters.observe(viewLifecycleOwner) {
            if (it == null) {
                return@observe
            }

            adapter.setContent(it)
        }

        viewModel.scrollState.observe(viewLifecycleOwner) { state ->
            when (state) {
                ScrollStateDef.LOADED -> {
                    scrollListener.loaded()
                }

                ScrollStateDef.RESUMED -> {
                    scrollListener.resumed()
                }

                ScrollStateDef.PAUSED -> {
                    scrollListener.paused()
                }

                ScrollStateDef.ENDED -> {
                    scrollListener.ended()
                }
            }
        }

        viewModel.requestCharacters(true)
    }

    private fun handleItemSelected(item: Character) {
        if (NetworkManager.isNetworkConnected()) {
            viewModel.setCharacter(item)
            val intent = Intent(requireContext(), DetailActivity::class.java)
            startActivity(intent)
        }
    }

    private fun handleData(isEmpty: Boolean) {
        val query = viewModel.query

        if (query.isEmpty() && isEmpty) {
            binding.layoutStart.containerStart.visibility = View.VISIBLE
            binding.layoutEmpty.containerEmpty.visibility = View.GONE
            binding.recyclerView.visibility = View.GONE
        } else if (isEmpty) {
            binding.layoutStart.containerStart.visibility = View.GONE
            binding.layoutEmpty.containerEmpty.visibility = View.VISIBLE
            binding.recyclerView.visibility = View.GONE
        } else {
            binding.layoutStart.containerStart.visibility = View.GONE
            binding.layoutEmpty.containerEmpty.visibility = View.GONE
            binding.recyclerView.visibility = View.VISIBLE
        }

        hideKeyboard()
    }

    private fun hideKeyboard() {
        if (isAdded) {
            val inputMethodManager =
                requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager

            val currentFocus = requireActivity().currentFocus ?: return
            inputMethodManager.hideSoftInputFromWindow(
                currentFocus.windowToken,
                InputMethodManager.RESULT_UNCHANGED_SHOWN
            )
        }
    }
}