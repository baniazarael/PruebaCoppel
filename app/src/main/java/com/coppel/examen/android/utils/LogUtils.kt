package com.coppel.examen.android.utils

import android.util.Log
import com.coppel.examen.android.BuildConfig

class LogUtils {
    companion object {
        fun print() {

        }

        fun print(text: String?) {
            if(BuildConfig.DEBUG) {
                if(text == null) {
                    return
                }

                if(text.isEmpty()) {
                    return
                }

                Log.d("Bani", text)
            }
        }

        fun print(TAG: String, text: String?) {
            if(BuildConfig.DEBUG) {
                if(TAG.isEmpty()) {
                    return
                }

                if(text == null) {
                    return
                }

                if(text.isEmpty()) {
                    return
                }

                Log.d(TAG, text)
            }
        }
    }
}