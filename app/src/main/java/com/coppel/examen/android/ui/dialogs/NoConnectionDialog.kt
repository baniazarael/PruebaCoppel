package com.coppel.examen.android.ui.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import com.coppel.examen.android.databinding.DialogNoConnectionBinding
import com.coppel.examen.android.utils.LogUtils

class NoConnectionDialog private constructor(
    context: Context
): Dialog(context) {
    private lateinit var binding: DialogNoConnectionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        binding = DialogNoConnectionBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnAccept.setOnClickListener {
            cancel()
        }
    }

    class Builder(context: Context) {
        private val dialog: NoConnectionDialog = NoConnectionDialog(context)

        fun create() {
            if(dialog.isShowing) {
                LogUtils.print("Dialog is showing")
            } else {
                dialog.show()
            }
        }
    }
}