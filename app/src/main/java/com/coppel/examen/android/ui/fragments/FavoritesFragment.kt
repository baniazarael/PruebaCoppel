package com.coppel.examen.android.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.coppel.examen.android.R
import com.coppel.examen.android.databinding.FragmentFavoritesBinding
import com.coppel.examen.android.interfaces.ItemListener
import com.coppel.examen.android.managers.NetworkManager
import com.coppel.examen.android.room.models.Favorite
import com.coppel.examen.android.ui.activities.DetailActivity
import com.coppel.examen.android.ui.adapters.FavoritesAdapter
import com.coppel.examen.android.viewModels.MainViewModel
import com.coppel.examen.android.viewModels.ViewModelFactory
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class FavoritesFragment: Fragment() {
    private lateinit var binding: FragmentFavoritesBinding

    private val viewModel by viewModels<MainViewModel> {
        ViewModelFactory()
    }

    private lateinit var adapter: FavoritesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentFavoritesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = FavoritesAdapter(object: ItemListener<Favorite> {
            override fun onItemSelected(view: View, item: Favorite) {
                if(view.id == R.id.containerImage) {
                    if(NetworkManager.isNetworkConnected()) {
                        viewModel.setFavorite(item)
                    }
                } else if(view.id == R.id.containerDelete) {
                    viewModel.delete(item)
                }
            }

            override fun onDataSet(isEmpty: Boolean, itemCount: Int) {
                if(isEmpty) {
                    binding.layoutStart.containerStart.visibility = View.VISIBLE
                    binding.recyclerView.visibility = View.GONE
                } else {
                    binding.layoutStart.containerStart.visibility = View.GONE
                    binding.recyclerView.visibility = View.VISIBLE
                }
            }
        })

        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        binding.recyclerView.adapter = adapter

        lifecycleScope.launch {
            viewModel.favorites.collectLatest { pagingData ->
                adapter.submitData(pagingData)
            }
        }

        viewModel.setCharacter(null)

        viewModel.character.observe(viewLifecycleOwner) {
            if(it == null) {
                return@observe
            }

            val intent = Intent(requireContext(), DetailActivity::class.java)
            startActivity(intent)
        }
    }
}