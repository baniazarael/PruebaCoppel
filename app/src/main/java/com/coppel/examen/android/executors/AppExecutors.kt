package com.coppel.examen.android.executors

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class AppExecutors {
    companion object {
        private val mainDispatcher: CoroutineDispatcher = Dispatchers.Main
        private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

        fun mainDispatcher(): CoroutineDispatcher {
            return mainDispatcher
        }

        fun ioDispatcher(): CoroutineDispatcher {
            return ioDispatcher
        }
    }
}