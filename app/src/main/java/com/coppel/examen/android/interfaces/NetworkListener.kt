package com.coppel.examen.android.interfaces

interface NetworkListener {
    suspend fun onConnected() {}
    suspend fun onDisconnected() {}
}