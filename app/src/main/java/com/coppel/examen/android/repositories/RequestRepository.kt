package com.coppel.examen.android.repositories

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.coppel.examen.android.executors.AppExecutors
import com.coppel.examen.android.interfaces.NetworkListener
import com.coppel.examen.android.managers.NetworkManager.NetworkStateDef
import com.coppel.examen.android.models.Character
import com.coppel.examen.android.retrofit.requests.CharacterRequest
import com.coppel.examen.android.retrofit.requests.CharactersRequest
import com.coppel.examen.android.retrofit.requests.SearchRequest
import com.coppel.examen.android.retrofit.responses.CharactersResponse
import com.coppel.examen.android.retrofit.responses.ErrorResponse
import com.coppel.examen.android.room.models.Favorite
import com.coppel.examen.android.utils.Constants
import com.coppel.examen.android.utils.Constants.ScrollStateDef
import kotlinx.coroutines.withContext

open class RequestRepository(context: Context): DaoRepository(context) {
    val characters: MutableLiveData<MutableList<Character>?> = MutableLiveData(mutableListOf())
    val character: MutableLiveData<Character?> = MutableLiveData(null)
    val scrollState = MutableLiveData(ScrollStateDef.DEFAULT)

    private var latestOffset: Int = Constants.INITIAL_OFFSET

    suspend fun setCharacter(result: Character?) = withContext(AppExecutors.mainDispatcher()) {
        character.value = result
    }

    private suspend fun setCharacters(result: MutableList<Character>) = withContext(AppExecutors.mainDispatcher()) {
        val items = characters.value ?: mutableListOf()

        if(result.isEmpty()) {
            items.clear()
        } else {
            items.addAll(result)
        }

        characters.value = items
    }

    suspend fun setScrollState(@ScrollStateDef result: Int) = withContext(AppExecutors.mainDispatcher()) {
        scrollState.value = result
    }

    private val networkListener: NetworkListener = object: NetworkListener {
        override suspend fun onConnected() {
            setUpdating(true)
            setNetworkState(NetworkStateDef.CONNECTED)
        }

        override suspend fun onDisconnected() {
            setUpdating(false)
            setNetworkState(NetworkStateDef.DISCONNECTED)
        }
    }

    suspend fun requestSearch(query: String) {
        setCharacters(mutableListOf())
        latestOffset = Constants.INITIAL_OFFSET
        setScrollState(ScrollStateDef.PAUSED)

        val params: HashMap<String, Any> = HashMap()
        params[Constants.JsonConstants.query] = query

        val request = SearchRequest()

        request.setNetworkListener(networkListener)

        when(val response = request.execute(params)) {
            is CharactersResponse -> {
                setUpdating(false)

                if(response.status == Constants.RESPONSE_OK) {
                    val results = response.data.results
                    setCharacters(results)
                } else {
                    setCharacters(mutableListOf())
                }
            }

            is ErrorResponse -> {
                setUpdating(false)
                setError(response.error)
            }
        }
    }

    suspend fun requestCharacter(id: String) {
        val params: HashMap<String, Any> = HashMap()
        params[Constants.JsonConstants.id] = id

        val request = CharacterRequest()

        request.setNetworkListener(networkListener)

        when(val response = request.execute(params)) {
            is CharactersResponse -> {
                setUpdating(false)

                if(response.status == Constants.RESPONSE_OK) {
                    val results = response.data.results
                    if(results.isNotEmpty()) {
                        setCharacter(results.first())
                    }
                } else {
                    setCharacters(mutableListOf())
                }

                setScrollState(ScrollStateDef.LOADED)
            }

            is ErrorResponse -> {
                setUpdating(false)
                setError(response.error)
            }
        }
    }

    suspend fun requestCharacters(init: Boolean = false) {
        if(init) {
            setCharacters(mutableListOf())
            latestOffset = Constants.INITIAL_OFFSET
        }

        val params: HashMap<String, Any> = HashMap()
        params[Constants.JsonConstants.offset] = latestOffset

        val request = CharactersRequest()

        request.setNetworkListener(networkListener)

        when(val response = request.execute(params)) {
            is CharactersResponse -> {
                setUpdating(false)

                if(response.status == Constants.RESPONSE_OK) {
                    val results = response.data.results
                    val offset = response.data.offset
                    latestOffset = offset + 1
                    setCharacters(results)
                } else {
                    setCharacters(mutableListOf())
                }

                setScrollState(ScrollStateDef.LOADED)
            }

            is ErrorResponse -> {
                setUpdating(false)
                setError(response.error)
            }
        }
    }

    fun fromCharacterToFavorite(character: Character): Favorite {
        return Favorite(
            character.id,
            character.name,
            character.thumbnail.getUrl(),
        )
    }
}