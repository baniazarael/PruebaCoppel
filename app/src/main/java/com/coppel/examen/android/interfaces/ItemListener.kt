package com.coppel.examen.android.interfaces

import android.view.View

interface ItemListener<Model> {
    fun onItemSelected(item: Model) {}

    fun onItemSelected(view: View, item: Model) {}

    fun onDataSet(isEmpty: Boolean, itemCount: Int) {}
}