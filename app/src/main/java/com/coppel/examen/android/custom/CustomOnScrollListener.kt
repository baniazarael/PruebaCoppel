package com.coppel.examen.android.custom

import androidx.recyclerview.widget.RecyclerView

abstract class CustomOnScrollListener: RecyclerView.OnScrollListener() {
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if(dx == 0 && dy == 0) {
            return
        }

        onScrolled()
    }

    abstract fun onScrolled()
}