package com.coppel.examen.android.managers

import android.app.Application
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.coppel.examen.android.R

class ImageManager {
    private val defaultOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .override(Target.SIZE_ORIGINAL)
            .dontTransform()

    private lateinit var thumbRequest: RequestBuilder<Drawable>
    private lateinit var errorRequest: RequestBuilder<Drawable>

    companion object {
        var instance = ImageManager()
    }

    fun initialize(application: Application) {
        this.errorRequest = Glide.with(application.applicationContext)
            .load(R.drawable.ic_error)
            .apply(getOptions())

        this.thumbRequest = Glide.with(application.applicationContext)
            .load(R.drawable.ic_loading)
            .apply(getOptions())
    }

    fun setImage(url: String, image: ImageView) {
        Glide.with(image)
            .load(url)
            .apply(getOptions())
            .thumbnail(thumbRequest)
            .error(errorRequest)
            .into(image)
    }

    private fun getOptions(): RequestOptions {
        return defaultOptions
    }
}