package com.coppel.examen.android.room.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.coppel.examen.android.room.RoomConstants
import java.util.*

@Entity(tableName = RoomConstants.tableFavorites)
data class Favorite(
    @PrimaryKey
    @ColumnInfo(name = RoomConstants.columnId)
    val id: String,

    @ColumnInfo(name = RoomConstants.columnName)
    val name: String,

    @ColumnInfo(name = RoomConstants.columnImageUrl)
    val imageUrl: String
) {
    override fun equals(other: Any?): Boolean {
        if(this === other) {
            return true
        }

        if(other is Favorite) {
            return id == other.id
        }

        return false
    }

    override fun hashCode(): Int {
        return Objects.hash(id, name, imageUrl)
    }
}