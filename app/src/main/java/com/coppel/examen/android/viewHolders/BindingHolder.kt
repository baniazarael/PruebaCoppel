package com.coppel.examen.android.viewHolders

import androidx.viewbinding.ViewBinding

class BindingHolder(
    val binding: ViewBinding
): ViewHolder(binding.root)