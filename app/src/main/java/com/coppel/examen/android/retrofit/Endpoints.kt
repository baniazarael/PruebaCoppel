package com.coppel.examen.android.retrofit

class Endpoints {
    companion object {
        const val baseUrl = "https://gateway.marvel.com"
        const val PUBLIC_KEY = "fa9fd52f5a38bc94db56cabb4e1a068f"
        const val PRIVATE_KEY = "c03a5af03d32e32e567de671e55ad14bb7d420d0"

        const val URL_CHARACTERS = "/v1/public/characters"
        const val URL_CHARACTERS_BY_ID = "/v1/public/characters/{id}"
        const val PARAM_ID = "id"
        const val PARAM_TS = "ts"
        const val PARAM_APIKEY = "apikey"
        const val PARAM_HASH = "hash"
        const val PARAM_LIMIT = "limit"
        const val PARAM_OFFSET = "offset"
        const val PARAM_NAME_STARTS_WITH = "nameStartsWith"
    }
}