package com.coppel.examen.android.managers

import android.app.Application
import android.content.Context
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat

class ResourceManager private constructor() {
    lateinit var application: Application

    companion object {
        @Volatile
        private var instance: ResourceManager? = null

        fun getInstance(): ResourceManager = instance ?: synchronized(this) {
            instance ?: ResourceManager().also {
                instance = it
            }
        }
    }

    init {
        if(instance != null) {
            throw RuntimeException("Use getInstance() method to get instance the single instance of this class.")
        }
    }

    fun initialize(application: Application) {
        this.application = application
    }

    fun getString(@StringRes id: Int): String {
        return getContext().resources.getString(id)
    }

    @ColorInt
    fun getColor(@ColorRes id: Int): Int {
        return ContextCompat.getColor(getContext(), id)
    }

    fun getContext(): Context {
        return application.applicationContext
    }
}