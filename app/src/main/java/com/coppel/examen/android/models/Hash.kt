package com.coppel.examen.android.models

import com.coppel.examen.android.retrofit.Endpoints
import com.coppel.examen.android.utils.HashUtils

class Hash {
    val timestamp = System.currentTimeMillis()

    fun getHash(): String {
        val content = "$timestamp${Endpoints.PRIVATE_KEY}${Endpoints.PUBLIC_KEY}"
        return HashUtils.hashMD5(content)
    }
}