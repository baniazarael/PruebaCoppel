package com.coppel.examen.android.base

import android.annotation.SuppressLint
import androidx.recyclerview.widget.RecyclerView
import com.coppel.examen.android.interfaces.ItemListener
import com.coppel.examen.android.viewHolders.BindingHolder

abstract class BaseAdapter<L>(
    var list: List<L>,

    open val listener: ItemListener<L>
): RecyclerView.Adapter<BindingHolder>() {
    constructor(listener: ItemListener<L>): this(emptyList(), listener)

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("NotifyDataSetChanged")
    open fun setContent(list: List<L>) {
        this.list = list
        notifyDataSetChanged()
        listener.onDataSet(list.isEmpty(), list.size)
    }
}