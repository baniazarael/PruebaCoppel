package com.coppel.examen.android.models

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Item(
    @SerializedName("name")
    val name: String,

    @SerializedName("resourceURI")
    val resourceURI: String
)