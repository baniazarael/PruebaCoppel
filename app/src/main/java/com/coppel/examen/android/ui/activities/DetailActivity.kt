package com.coppel.examen.android.ui.activities

import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabColorSchemeParams
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.coppel.examen.android.R
import com.coppel.examen.android.databinding.ActivityDetailBinding
import com.coppel.examen.android.interfaces.ItemListener
import com.coppel.examen.android.managers.ImageManager
import com.coppel.examen.android.models.Character
import com.coppel.examen.android.models.DetailItem
import com.coppel.examen.android.ui.adapters.DetailAdapter
import com.coppel.examen.android.utils.Constants.FavoriteStateDef
import com.coppel.examen.android.viewModels.MainViewModel
import com.coppel.examen.android.viewModels.ViewModelFactory

class DetailActivity: AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding

    private val viewModel by viewModels<MainViewModel> {
        ViewModelFactory()
    }

    private lateinit var adapter: DetailAdapter

    private var character: Character? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.layoutToolbar.toolbar)
        val actionBar = supportActionBar

        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeButtonEnabled(true)
        }

        adapter = DetailAdapter(object: ItemListener<DetailItem> {
            override fun onItemSelected(item: DetailItem) {
                val url = item.url?.replace("http:", "https:") ?: return
                handleUrl(url)
            }
        })

        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter

        viewModel.character.observe(this) {
            if(it == null) {
                return@observe
            }

            character = it
            handleCharacter(it)
        }

        viewModel.favoriteState.observe(this) {
            @FavoriteStateDef
            when(it) {
                FavoriteStateDef.INVALIDATE_MENU -> {
                    invalidateOptionsMenu()
                }

                FavoriteStateDef.ADD_FAVORITE -> {
                    Toast.makeText(this@DetailActivity, R.string.textAddedFavorites, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_detail, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val isFavoriteActive = viewModel.isFavoriteActive()

        if(isFavoriteActive) {
            disableMenu(menu)
        } else {
            enableMenu(menu)
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val character = this.character ?: return false

        if(item.itemId == R.id.actionFavorite) {
            viewModel.addFavorite(character)
            return true
        }

        return false
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    private fun enableMenu(menu: Menu) {
        menu.findItem(R.id.actionFavorite).isEnabled = true
        menu.findItem(R.id.actionFavorite).isVisible = true
    }

    private fun disableMenu(menu: Menu) {
        menu.findItem(R.id.actionFavorite).isEnabled = false
        menu.findItem(R.id.actionFavorite).isVisible = false
    }

    private fun handleUrl(url: String) {
        val uri: Uri = Uri.parse(url)
        val intentBuilder = CustomTabsIntent.Builder()
        val params = CustomTabColorSchemeParams.Builder()
            .setNavigationBarColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .setSecondaryToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .build()

        intentBuilder.setColorSchemeParams(CustomTabsIntent.COLOR_SCHEME_DARK, params)
        intentBuilder.setStartAnimations(this, R.anim.slide_in_right, R.anim.slide_out_left)
        intentBuilder.setExitAnimations(this, android.R.anim.slide_in_left, android.R.anim.slide_out_right)

        val customTabsIntent = intentBuilder.build()
        customTabsIntent.launchUrl(this, uri)
    }

    private fun handleCharacter(character: Character) {
        ImageManager.instance.setImage(character.thumbnail.getUrl(), binding.ivImage)
        adapter.setContent(viewModel.getInfo(character))
        binding.layoutToolbar.tvTitle.text = character.name

        viewModel.isFavorite(character.id).observe(this) {
            val active = it != null
            viewModel.setFavoriteActive(active)
        }
    }
}