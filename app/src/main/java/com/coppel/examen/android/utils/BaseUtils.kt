package com.coppel.examen.android.utils

import java.text.SimpleDateFormat
import java.util.*

class BaseUtils {
    companion object {
        fun getTimestamp(): Long {
            return System.currentTimeMillis()
        }

        fun getDateTimeFormatted(timestamp: Long): String {
            val format = SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.getDefault())
            return format.format(Date(timestamp))
        }
    }
}