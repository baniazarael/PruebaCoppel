package com.coppel.examen.android.repositories

import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.coppel.examen.android.broadcasts.NetworkStateReceiver
import com.coppel.examen.android.interfaces.NetworkListener
import com.coppel.examen.android.managers.NetworkManager
import com.coppel.examen.android.managers.NetworkManager.NetworkStateDef
import com.coppel.examen.android.utils.LogUtils

open class NetworkRepository(context: Context): RequestRepository(context) {
    private val network = MutableLiveData(NetworkStateDef.DEFAULT)

    private val networkCallback: ConnectivityManager.NetworkCallback = object: ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            LogUtils.print("onConnected N")

            NetworkManager.setConnected(true)
            setNetwork(NetworkStateDef.CONNECTED)
        }

        override fun onLost(network: Network) {
            LogUtils.print("onDisconnected N")

            NetworkManager.setConnected(false)
            setNetwork(NetworkStateDef.DISCONNECTED)
        }
    }

    init {
        registerNetwork(context)
    }

    fun getNetworkState(): LiveData<Int> {
        return network
    }

    fun setNetwork(@NetworkStateDef network: Int) {
        this.network.postValue(network)
    }

    private fun registerNetwork(context: Context) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            LogUtils.print("register registerNetwork Android > Nougat")

            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            connectivityManager.registerDefaultNetworkCallback(networkCallback)
        } else {
            LogUtils.print("register registerNetwork Android < MarshMallow")

            val networkStateReceiver = NetworkStateReceiver(context, object: NetworkListener {
                override suspend fun onConnected() {
                    LogUtils.print("onConnected M")
                    NetworkManager.setConnected(true)
                    setNetwork(NetworkStateDef.CONNECTED)
                }

                override suspend fun onDisconnected() {
                    LogUtils.print("onDisconnected M")
                    NetworkManager.setConnected(false)
                    setNetwork(NetworkStateDef.DISCONNECTED)
                }
            })

            networkStateReceiver.init()
            context.registerReceiver(networkStateReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        }
    }

    private fun unregisterNetwork(context: Context) {
        LogUtils.print("register unregisterNetwork")

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager.unregisterNetworkCallback(networkCallback)
        }
    }
}