package com.coppel.examen.android.repositories

import androidx.annotation.NonNull
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingConfig
import com.coppel.examen.android.executors.AppExecutors
import com.coppel.examen.android.managers.NetworkManager.NetworkStateDef
import kotlinx.coroutines.withContext

abstract class BaseRepository protected constructor() {
    val updating = MutableLiveData(false)
    val error = MutableLiveData<Exception?>(null)
    val networkState = MutableLiveData(NetworkStateDef.DEFAULT)

    protected val pagingConfig = PagingConfig(60)

    suspend fun setUpdating(result: Boolean) = withContext(AppExecutors.mainDispatcher()) {
        updating.value = result
    }

    suspend fun setError(result: Exception) = withContext(AppExecutors.mainDispatcher()) {
        error.value = result
        error.postValue(null)
    }

    suspend fun setNetworkState(@NetworkStateDef result: Int) = withContext(AppExecutors.mainDispatcher()) {
        networkState.value = result
        networkState.postValue(NetworkStateDef.DEFAULT)
    }

    @NonNull
    fun isUpdating(): Boolean {
        return updating.value ?: false
    }
}