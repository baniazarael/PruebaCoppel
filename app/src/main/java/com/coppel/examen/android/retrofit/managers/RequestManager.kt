package com.coppel.examen.android.retrofit.managers

import com.coppel.examen.android.executors.AppExecutors
import com.coppel.examen.android.retrofit.interceptor.RetrofitInterceptor
import com.coppel.examen.android.retrofit.responses.NetworkResponse
import com.coppel.examen.android.retrofit.responses.Response
import com.coppel.examen.android.utils.LogUtils
import kotlinx.coroutines.withContext
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

abstract class RequestManager(
    TAG: String,

    baseUrl: String
) {
    companion object {
        const val DEFAULT_TIMEOUT = 15L
    }

    protected val retrofit: Retrofit

    init {
        LogUtils.print("RequestName", TAG + "Request")

        val okHttpBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
        val interceptor: Interceptor = RetrofitInterceptor(TAG)

        okHttpBuilder.addInterceptor(interceptor)
        okHttpBuilder.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
        okHttpBuilder.callTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
        okHttpBuilder.readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
        okHttpBuilder.writeTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)

        val okHttpClient: OkHttpClient = okHttpBuilder.build()

        val builder = Retrofit.Builder()
        builder.baseUrl(baseUrl)
        builder.client(okHttpClient)
        builder.addConverterFactory(GsonConverterFactory.create())

        this.retrofit = builder.build()
    }

    protected abstract suspend fun onPreExecute(): Boolean
    protected abstract suspend fun onExecute(params: HashMap<String, Any>): Response

    suspend fun execute(params: HashMap<String, Any> = HashMap()): Response {
        val networkAvailable = withContext(AppExecutors.mainDispatcher()) {
            onPreExecute()
        }

        return if(networkAvailable) {
            val result = withContext(AppExecutors.ioDispatcher()) {
                onExecute(params) // runs in background thread without blocking the Main Thread
            }

            result
        } else {
            NetworkResponse()
        }
    }
}