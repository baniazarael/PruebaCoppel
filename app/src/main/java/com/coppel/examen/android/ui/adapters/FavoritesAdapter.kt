package com.coppel.examen.android.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.coppel.examen.android.base.PagingAdapter
import com.coppel.examen.android.databinding.ItemFavoriteBinding
import com.coppel.examen.android.interfaces.ItemListener
import com.coppel.examen.android.managers.ImageManager
import com.coppel.examen.android.models.diff.DiffUtils
import com.coppel.examen.android.room.models.Favorite
import com.coppel.examen.android.viewHolders.BindingHolder

class FavoritesAdapter(listener: ItemListener<Favorite>): PagingAdapter<Favorite>(DiffUtils.FAVORITE_ITEM_CALLBACK, listener) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingHolder {
        val binding = ItemFavoriteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BindingHolder(binding)
    }

    override fun onBindViewHolder(holder: BindingHolder, position: Int) {
        val item = getItem(position) ?: return

        if(holder.binding is ItemFavoriteBinding) {
            val binding: ItemFavoriteBinding = holder.binding

            ImageManager.instance.setImage(item.imageUrl, binding.ivImage)

            binding.tvName.text = item.name

            binding.containerImage.setOnClickListener {
                listener.onItemSelected(it, item)
            }

            binding.containerDelete.setOnClickListener {
                listener.onItemSelected(it, item)
            }
        }
    }
}