package com.coppel.examen.android.custom

import android.os.Handler
import android.os.Looper
import androidx.recyclerview.widget.LinearLayoutManager
import com.coppel.examen.android.interfaces.LoadListener
import com.coppel.examen.android.utils.Constants.ScrollStateDef

class InfiniteScrollListener(
    private val linearLayoutManager: LinearLayoutManager,
    private val listener: LoadListener
): CustomOnScrollListener() {
    companion object {
        private const val VISIBLE_THRESHOLD = 1
    }

    private var ended = false
    private var loading = false
    private var pauseScrolling = false

    @ScrollStateDef
    var lastState = ScrollStateDef.DEFAULT

    override fun onScrolled() {
        val totalItemCount = linearLayoutManager.itemCount
        val lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
        if(!loading && totalItemCount <= lastVisibleItem + VISIBLE_THRESHOLD && totalItemCount != 0 && !ended && !pauseScrolling) {
            listener.onLoadMore()
            loading = true
        }
    }

    fun loaded() {
        Handler(Looper.getMainLooper()).postDelayed({
            loading = false
            lastState = ScrollStateDef.LOADED
        }, 200)
    }

    fun paused() {
        pauseScrolling = true
        lastState = ScrollStateDef.PAUSED
    }

    fun resumed() {
        pauseScrolling = false
        lastState = ScrollStateDef.RESUMED
    }

    fun ended() {
        ended = true
        lastState = ScrollStateDef.ENDED
    }
}