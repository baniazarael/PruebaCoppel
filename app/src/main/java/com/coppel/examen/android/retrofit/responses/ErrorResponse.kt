package com.coppel.examen.android.retrofit.responses

data class ErrorResponse(
    val error: Exception
): Response()