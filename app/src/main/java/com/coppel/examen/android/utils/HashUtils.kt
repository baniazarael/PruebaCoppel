package com.coppel.examen.android.utils

import org.apache.commons.codec.digest.DigestUtils

class HashUtils {
    companion object {
        fun hashMD5(data: String): String {
            return DigestUtils.md5Hex(data)
        }
    }
}