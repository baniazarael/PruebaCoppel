package com.coppel.examen.android.retrofit

class RetrofitConstants {
    companion object {
        const val request = "Request"
        const val response = "Response"
        const val responseBodyError = "Response body is null"
    }
}