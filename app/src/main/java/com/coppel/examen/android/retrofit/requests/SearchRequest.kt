package com.coppel.examen.android.retrofit.requests

import com.coppel.examen.android.models.Hash
import com.coppel.examen.android.retrofit.RetrofitConstants
import com.coppel.examen.android.retrofit.base.RetrofitRequest
import com.coppel.examen.android.retrofit.responses.Response
import com.coppel.examen.android.retrofit.services.ApiService
import com.coppel.examen.android.utils.Constants
import com.coppel.examen.android.utils.Constants.JsonConstants
import java.io.IOException

class SearchRequest: RetrofitRequest(
    Constants.R_SEARCH
) {
    override suspend fun onExecute(params: HashMap<String, Any>): Response {
        try {
            val query: String = params[JsonConstants.query] as String

            val hash = Hash()
            val service: ApiService = retrofit.create(ApiService::class.java)
            val response = service.search(ts = hash.timestamp, hash = hash.getHash(), nameStartsWith = query)

            val responseBody = response.body() ?: return onError(RuntimeException(TAG + " " + RetrofitConstants.responseBodyError))

            return if(response.isSuccessful) {
                responseBody
            } else {
                return onError(response.body(), response.code())
            }
        } catch (ex: IOException) {
            return onError(ex)
        }
    }
}