package com.coppel.examen.android.room

class RoomConstants {
    companion object {
        const val databaseName = "Marvel.db"
        const val databaseVersion = 1

        const val tableFavorites = "tableFavorites"

        // Favorite
        const val columnId = "id"
        const val columnName = "name"
        const val columnImageUrl = "imageUrl"
    }
}