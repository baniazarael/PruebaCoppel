package com.coppel.examen.android.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.coppel.examen.android.R
import com.coppel.examen.android.base.BaseAdapter
import com.coppel.examen.android.databinding.ItemHomeBinding
import com.coppel.examen.android.interfaces.ItemListener
import com.coppel.examen.android.managers.ImageManager
import com.coppel.examen.android.models.Character
import com.coppel.examen.android.viewHolders.BindingHolder

class HomeAdapter(listener: ItemListener<Character>): BaseAdapter<Character>(listener) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingHolder {
        val binding = ItemHomeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BindingHolder(binding)
    }

    override fun onBindViewHolder(bindingHolder: BindingHolder, position: Int) {
        if(bindingHolder.binding is ItemHomeBinding) {
            val binding = bindingHolder.binding
            val item: Character = list[position]

            binding.tvName.text = item.name

            val description = item.description

            if(description.isEmpty()) {
                binding.tvDescription.setText(R.string.textNoInformation)
            } else {
                binding.tvDescription.text = description
            }

            ImageManager.instance.setImage(item.thumbnail.getUrl(), binding.ivImage)

            binding.container.setOnClickListener {
                listener.onItemSelected(item)
            }

            binding.ivImage.setOnClickListener {
                listener.onItemSelected(item)
            }
        }
    }
}