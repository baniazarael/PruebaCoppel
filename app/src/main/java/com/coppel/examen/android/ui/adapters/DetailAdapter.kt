package com.coppel.examen.android.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.coppel.examen.android.base.BaseAdapter
import com.coppel.examen.android.databinding.ItemContentBinding
import com.coppel.examen.android.databinding.ItemDividerBinding
import com.coppel.examen.android.databinding.ItemHeaderBinding
import com.coppel.examen.android.interfaces.ItemListener
import com.coppel.examen.android.models.DetailItem
import com.coppel.examen.android.utils.Constants.ItemDef
import com.coppel.examen.android.viewHolders.BindingHolder

class DetailAdapter(
    listener: ItemListener<DetailItem>
): BaseAdapter<DetailItem>(listener) {
    override fun getItemViewType(position: Int): Int {
        val item = list[position]
        val id = item.id

        if(id == ItemDef.HEADER) {
            return ItemDef.HEADER
        } else if(id == ItemDef.DIVIDER) {
            return ItemDef.DIVIDER
        }

        return ItemDef.CONTENT
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingHolder {
        return when (viewType) {
            ItemDef.HEADER -> {
                val binding = ItemHeaderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                BindingHolder(binding)
            }

            ItemDef.DIVIDER -> {
                val binding = ItemDividerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                BindingHolder(binding)
            }

            else -> {
                val binding = ItemContentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                BindingHolder(binding)
            }
        }
    }

    override fun onBindViewHolder(bindingHolder: BindingHolder, position: Int) {
        val item = list[position]

        when(bindingHolder.binding) {
            is ItemHeaderBinding -> {
                val holder = bindingHolder.binding

                holder.tvTitle.text = item.title
            }

            is ItemContentBinding -> {
                val holder = bindingHolder.binding

                holder.tvTitle.text = item.title

                holder.tvText.text = item.text

                holder.ivImage.setImageResource(item.icon)

                holder.container.setOnClickListener {
                    listener.onItemSelected(item)
                }
            }
        }
    }
}