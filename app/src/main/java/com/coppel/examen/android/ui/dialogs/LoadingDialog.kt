package com.coppel.examen.android.ui.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import com.coppel.examen.android.databinding.DialogLoadingBinding

class LoadingDialog private constructor(
    context: Context,
): Dialog(context) {
    companion object {
        const val ROTATE_ANIMATION_DURATION = 800
    }

    private val rotate = RotateAnimation(
        0f, 360f,
        Animation.RELATIVE_TO_SELF, 0.5f,
        Animation.RELATIVE_TO_SELF, 0.5f
    )

    private lateinit var binding: DialogLoadingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setCancelable(false)

        binding = DialogLoadingBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun show() {
        super.show()

        binding.progressBar.startAnimation(getRotation())
    }

    override fun cancel() {
        super.cancel()

        binding.progressBar.clearAnimation()
    }

    fun getRotation(): RotateAnimation {
        rotate.duration = ROTATE_ANIMATION_DURATION.toLong()
        rotate.repeatCount = Animation.INFINITE

        return rotate
    }

    class Builder(context: Context) {
        private val dialog: LoadingDialog = LoadingDialog(context)

        fun create() {
            if(dialog.isShowing) {
                Log.d("Bani", "Mostrando dialogo")
            } else {
                dialog.show()
            }
        }

        fun cancel() {
            if(dialog.isShowing) {
                dialog.cancel()
            }
        }
    }
}