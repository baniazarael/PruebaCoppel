package com.coppel.examen.android.models

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
class Thumbnail(
    @SerializedName("path")
    val path: String,

    @SerializedName("extension")
    val extension: String
) {
    fun getUrl(): String {
        return "$path.$extension"
    }
}