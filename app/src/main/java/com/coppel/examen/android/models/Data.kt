package com.coppel.examen.android.models

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Data(
    @SerializedName("offset")
    val offset: Int,

    @SerializedName("results")
    val results: MutableList<Character>
)