package com.coppel.examen.android.models

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Character(
    @SerializedName("id")
    val id: String,

    @SerializedName("name")
    val name: String,

    @SerializedName("description")
    val description: String,

    @SerializedName("thumbnail")
    val thumbnail: Thumbnail,

    @SerializedName("comics")
    val comics: Items,

    @SerializedName("series")
    val series: Items,

    @SerializedName("stories")
    val stories: Items,

    @SerializedName("events")
    val events: Items,

    @SerializedName("urls")
    val urls: MutableList<Url>
)